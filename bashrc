# scp bashrc target-host:.bash_dmytro
# target-host% echo ". ~/.bash_dmytro" >> .bashrc

# if [ -x /usr/bin/dircolors -a -z $LS_COLORS ]
# then
#   eval "$(dircolors -b)"
# fi

unset TMOUT
export -n TMOUT

export EDITOR=vim

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
