setopt autocd
setopt extended_glob
setopt NO_NOMATCH
setopt correct

PS1='%T%# ' 
PS2='> '
RPROMPT='%~'

autoload -U compinit
compinit -C

zstyle ':completion:*' menu yes select

eval `dircolors`
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

[ -z "`which symfony-autocomplete`" ] && eval "`symfony-autocomplete`"

case $TERM in
  screen*)
    precmd () { print -Pn "\033k%~\033\\" }
    preexec () { print -Pn "\033k$1\033\\" }
    ;;
  *xterm*|rxvt|rxvt-unicode|rxvt-unicode-256color)
    precmd () { print -Pn "\e]0;%n@%m: %~\a" }
    preexec () { print -Pn "\e]0;%n@%m: $1\a" }
    ;;
esac

export EDITOR=vim

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
