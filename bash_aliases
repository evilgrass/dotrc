alias ls='ls --color=auto --group-directories-first'
alias grep='grep --color=auto'
alias less='less -x4'

alias f=feh
alias fz="feh -Z"

alias ..="cd .."
alias sdr='screen -aAdr'
alias la='ls -A'
alias ll='ls -lA'
alias gci='git add -A && git commit -m up'
alias today='vim `date +%Y-%m-%d.txt`'
