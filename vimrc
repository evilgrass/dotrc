
" https://github.com/bogado/file-line

" mkdir -p ~/.vim/plugin && cd ~/.vim/plugin
" wget https://raw.githubusercontent.com/bogado/file-line/master/plugin/file_line.vim

set nocompatible
syntax on
colorscheme default
set background=dark
set nobackup
set noswapfile
set ruler
set acd
set modeline
set noerrorbells
set novisualbell

set number
imap <F11> <Esc>:set<Space>nu!<CR>a
nmap <F11> :set<Space>nu!<CR>

set tabstop=4
set expandtab
if expand("%:t") =~ "Makefile"
  set noexpandtab
endif

set lbr

autocmd BufWritePost * call CheckScript()
function CheckScript()
  if getline(1) =~ "^#!.*/bin/"
    silent !chmod +x <afile>
  endif
endfunction

